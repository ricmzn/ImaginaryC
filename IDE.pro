#-------------------------------------------------
#
# Project created by QtCreator 2016-03-29T19:21:18
#
#-------------------------------------------------

QT += core gui widgets
CONFIG += c++11

TARGET = ImaginaryC
TEMPLATE = app

INCLUDEPATH += Source \
    "Third Party/boost 1.61.0/include"

SOURCES += Source/Main.cpp \
    Source/Widgets/EditorWindow.cpp \
    Source/Widgets/ConsoleWindow.cpp \
    Source/Compiler/Parser/Constants.cpp \
    Source/Compiler/Parser/Lexico.cpp \
    Source/Compiler/Parser/Semantico.cpp \
    Source/Compiler/Parser/Sintatico.cpp \
    Source/Widgets/ICSyntaxHighlighter.cpp \
    Source/Widgets/CodeEditor.cpp \
    Source/Compiler/BaseCompiler.cpp \
    Source/Compiler/Symbol.cpp \
    Source/Widgets/ResultsWindow.cpp \
    Source/Compiler/ScopeTree.cpp \
    Source/Widgets/BipAsmSyntaxHighlighter.cpp \
    Source/CompilerFactories.cpp \
    Source/Compiler/BipCompiler.cpp \
    Source/Compiler/Instruction.cpp \
    Source/Compiler/Expression.cpp

HEADERS += Source/Widgets/EditorWindow.h \
    Source/Widgets/ConsoleWindow.h \
    Source/Compiler/Parser/AnalysisError.h \
    Source/Compiler/Parser/Constants.h \
    Source/Compiler/Parser/LexicalError.h \
    Source/Compiler/Parser/Lexico.h \
    Source/Compiler/Parser/SemanticError.h \
    Source/Compiler/Parser/Semantico.h \
    Source/Compiler/Parser/Sintatico.h \
    Source/Compiler/Parser/SyntaticError.h \
    Source/Compiler/Parser/Token.h \
    Source/Widgets/ICSyntaxHighlighter.h \
    Source/Widgets/CodeEditor.h \
    Source/Compiler/BaseCompiler.h \
    Source/Compiler/Symbol.h \
    Source/Widgets/ResultsWindow.h \
    Source/Compiler/ScopeTree.h \
    Source/Compiler/LogPrinter.h \
    Source/Widgets/BipAsmSyntaxHighlighter.h \
    Source/CompilerFactories.h \
    Source/Compiler/BipCompiler.h \
    Source/Compiler/Instruction.h \
    Source/Compiler/Expression.h

FORMS   += Source/Widgets/EditorWindow.ui \
    Source/Widgets/ResultsWindow.ui

RESOURCES += \
    Icons/icons.qrc
