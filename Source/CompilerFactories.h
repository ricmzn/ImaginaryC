#ifndef COMPILERFACTORIES_H
#define COMPILERFACTORIES_H

#include <memory>
#include <vector>
#include <QObject>

#include "Compiler/BaseCompiler.h"

class BaseCompilerFactory : public QObject
{
    Q_OBJECT

public:
    BaseCompilerFactory(QObject* parent = 0);
    virtual std::shared_ptr<BaseCompiler> create() const;
    virtual const char* getName() const;
    virtual const char* getDescription() const;
};
Q_DECLARE_METATYPE(BaseCompilerFactory*)


class BipCompilerFactory : public BaseCompilerFactory
{
    Q_OBJECT

public:
    BipCompilerFactory(QObject* parent = 0);
    virtual std::shared_ptr<BaseCompiler> create() const override;
    virtual const char* getName() const override;
    virtual const char* getDescription() const override;
};
Q_DECLARE_METATYPE(BipCompilerFactory*)

namespace CompilerFactories {
    typedef std::shared_ptr<BaseCompilerFactory> CompilerFactoryPtr;
    typedef std::vector<CompilerFactoryPtr> CompilerFactoryList;
    const CompilerFactoryList& get();
}

#endif // COMPILERFACTORIES_H
