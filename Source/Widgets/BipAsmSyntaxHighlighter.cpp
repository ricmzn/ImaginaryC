#include "BipAsmSyntaxHighlighter.h"

struct HighlightRule {
    QRegExp regex;
    QString tag;
    QColor color;
    bool italic;
    bool bold;
};

static QTextCharFormat ruleToFormat(const HighlightRule& rule)
{
    QTextCharFormat format;
    int weight = rule.bold ? QFont::Bold : QFont::Normal;
    format.setForeground(rule.color);
    format.setFontItalic(rule.italic);
    format.setFontWeight(weight);
    return format;
}

#define RULE(regexp, color, italic, bold, tag)\
HighlightRule {\
    QRegExp(regexp),\
    QString(tag),\
    QColor(color),\
    bool(italic),\
    bool(bold)\
}

static HighlightRule highlightRules[] = {
    // Comentários
    RULE("#.*", Qt::darkGreen, true, false, ""),
    // Números inteiros
    RULE("\\b[0-9]+\\b", Qt::darkCyan, false, false, ""),
    // Endereços especiais
    RULE("\\$.*", Qt::darkCyan, false, false, ""),
    // Marcadores de segmento
    RULE("(\\.data|\\.text)", Qt::darkMagenta, false, true, ""),
    // Marcadores de desvio
    RULE("^.+:$", Qt::black, true, false, ""),
    // Instruções BIP I
    RULE("\\b(hlt|sto|ld|ldi|add|addi|sub|subi)\\b", Qt::darkBlue, false, true, ""),
    // Instruções BIP II
    RULE("\\b(beq|bne|bgt|bge|blt|ble|jmp)\\b", Qt::darkBlue, false, true, ""),
    // Instruções BIP III
    RULE("\\b(not|and|andi|or|ori|xor|xori|sll|slr)\\b", Qt::darkBlue, false, true, ""),
    // Instruções BIP IV
    RULE("\\b(stov|ldv|return|call)\\b", Qt::darkBlue, false, true, ""),
};

#undef RULE

/*****************/
/* COMEÇA CÓDIGO */
/*****************/

BipAsmSyntaxHighlighter::BipAsmSyntaxHighlighter(QObject* parent)
    : QSyntaxHighlighter(parent)
{}

BipAsmSyntaxHighlighter::~BipAsmSyntaxHighlighter()
{}

void BipAsmSyntaxHighlighter::highlightBlock(const QString& text)
{
    bool matched = false;
    for(HighlightRule rule : highlightRules) {
        rule.regex.setCaseSensitivity(Qt::CaseInsensitive);
        const auto& format = ruleToFormat(rule);
        int index = rule.regex.indexIn(text);
        while(index >= 0) {
            int length = rule.regex.matchedLength();
            setFormat(index, length, format);
            index = rule.regex.indexIn(text, index + length);
            matched = true;
        }
    }
    if(!matched) {
        setFormat(0, text.size(), Qt::red);
    }
}
