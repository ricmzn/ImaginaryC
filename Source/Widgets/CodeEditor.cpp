#include "CodeEditor.h"
#include <QKeyEvent>
#include <QTextBlock>

#ifdef __WIN32
    static QFont monospace("Courier", 10);
#else
    static QFont monospace("Liberation Mono", 10);
#endif

CodeEditor::CodeEditor(QWidget* parent)
    : QTextEdit(parent)
    , tabSpaces(4)
{
    setFont(monospace);
}

CodeEditor::~CodeEditor()
{}

void CodeEditor::keyPressEvent(QKeyEvent* event)
{
    if(isReadOnly()) {
        QTextEdit::keyPressEvent(event);
        return;
    }
    auto cur = textCursor();
    int cursorPos = cur.position() - cur.block().position();
    switch(event->key()) {
        // Tab: adiciona até 4 espaços
        case Qt::Key_Tab: {
            int numSpaces = tabSpaces - (cursorPos % tabSpaces);
            cur.insertText(QString(numSpaces, ' '));
            break;
        }
        // Backspace: remove até 4 espaços
        case Qt::Key_Backspace: {
            int numSpaces = cursorPos % tabSpaces;
            if(numSpaces == 0) {
                numSpaces = tabSpaces;
            }
            if(!cur.hasSelection()) {
                // Verifica se não há nenhum caractere desde o início da linha
                bool blank = true;
                int pos = 0;
                while(pos < cursorPos) {
                    QChar ch = toPlainText().at(pos +cur.block().position());
                    if(ch != ' ') {
                        blank = false;
                        break;
                    }
                    pos++;
                }
                if(blank) {
                    do {
                        cur.deletePreviousChar();
                        --numSpaces;
                    }
                    while(numSpaces > 0 && lastChar(cur) == ' ');
                }
                else {
                    cur.deletePreviousChar();
                }
            }
            else {
                cur.removeSelectedText();
            }
            break;
        }
        // Outras teclas tratadas normalmente
        default: {
            QTextEdit::keyPressEvent(event);
            break;
        }
    }
}

void CodeEditor::keyReleaseEvent(QKeyEvent* event)
{
    QTextEdit::keyReleaseEvent(event);
}

QChar CodeEditor::lastChar(const QTextCursor& cur) const
{
    if(cur.position() > 0) {
        return toPlainText().at(cur.position() - 1);
    }
    return '\0';
}
