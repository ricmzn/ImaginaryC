#ifndef SYMBOLSWINDOW_H
#define SYMBOLSWINDOW_H

#include <QWidget>
#include "Compiler/ScopeTree.h"

class BaseCompiler;
class QTableWidgetItem;
class QTreeWidgetItem;

namespace Ui {
class ResultsWindow;
}

class ResultsWindow : public QWidget
{
    Q_OBJECT

public:
    explicit ResultsWindow(QWidget* parent = 0);
    ~ResultsWindow();

    void copyDataFrom(const BaseCompiler* semantic);
    void setAssemblyText(const QString& text);

public slots:
    void clear();

private slots:
    void actionSave();

private:
    Ui::ResultsWindow* ui;
    QTableWidgetItem* newTableItem(const std::string& text) const;
    QTreeWidgetItem* newTreeItem(const std::string& path, const std::string& tag) const;
    QTreeWidgetItem* addRecursive(ScopeTreeNode* node) const;
};

#endif // SYMBOLSWINDOW_H
