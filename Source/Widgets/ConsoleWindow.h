#ifndef CONSOLEWINDOW_H
#define CONSOLEWINDOW_H

#include <memory>
#include "Compiler/LogPrinter.h"
#include "Widgets/CodeEditor.h"

class ConsoleWindow : public CodeEditor
{
    Q_OBJECT

    struct ConsolePrinter : public ILogPrinter
    {
        ConsoleWindow* console;
        ConsolePrinter(ConsoleWindow* console);
        void logWarning(const char* message) override;
    };

    std::shared_ptr<ConsolePrinter> printer;

public:
    enum MessageType {
        Normal,
        Success,
        Info,
        Warning,
        Error
    };

    explicit ConsoleWindow(QWidget* parent = 0);
    ~ConsoleWindow();

    void breakLine();
    void showMessage(QString prefix, QString text, MessageType type = Normal);
    void showFormattedString(QString text, QColor color, int weight = QFont::Normal);
    void moveToEnd();

    std::shared_ptr<ILogPrinter> getPrinter();
};

#endif // CONSOLEWINDOW_H
