#include "ResultsWindow.h"
#include "ui_ResultsWindow.h"

#include <QMessageBox>
#include <QFileDialog>

#include "Compiler/BaseCompiler.h"
#include "Widgets/BipAsmSyntaxHighlighter.h"

ResultsWindow::ResultsWindow(QWidget* parent)
    : QWidget(parent)
    , ui(new Ui::ResultsWindow)
{
    static QStringList tableHeader {"Identificador", "Tipo", "Escopo", "Vetor", "Função", "Parâmetro", "Inicializada", "Usada"};

    ui->setupUi(this);
    ui->tableWidget->setSelectionBehavior(QAbstractItemView::SelectRows);
    ui->tableWidget->setColumnCount(tableHeader.size());
    ui->tableWidget->setHorizontalHeaderLabels(tableHeader);
    ui->tableWidget->horizontalHeader()->setSectionResizeMode(QHeaderView::Stretch);
    ui->treeWidget->setSelectionBehavior(QAbstractItemView::SelectRows);
    ui->treeWidget->setHeaderLabels({"Caminho", "Tipo"});
    ui->treeWidget->header()->setSectionResizeMode(QHeaderView::Stretch);

    // Configura a aba de assembly
    auto asmHighlighter = new BipAsmSyntaxHighlighter(ui->assemblyView);
    asmHighlighter->setDocument(ui->assemblyView->document());
    ui->tabWidget->setTabEnabled(1, false);

    // Move o botão de salvar para dentro do editor de texto
    ui->gridLayout->removeWidget(ui->saveButton);
    ui->gridLayout->addWidget(ui->saveButton, 0, 0, Qt::AlignRight | Qt::AlignBottom);
    ui->saveButton->raise();

    // Permite que essa tela seja uma janela independente
    setWindowFlags(Qt::Window);

    // Sinais
    connect(ui->saveButton, SIGNAL(clicked()), this, SLOT(actionSave()));
}

ResultsWindow::~ResultsWindow()
{
    delete ui;
}

void ResultsWindow::copyDataFrom(const BaseCompiler* semantic)
{
    clear();
    auto symbols = semantic->getSymbols();
    ui->tableWidget->setRowCount(symbols.size());
    for(int i = 0; i < int(symbols.size()); i++) {
        const Symbol& sym = symbols[i];
        ui->tableWidget->setItem(i, 0, newTableItem(sym.name));
        ui->tableWidget->setItem(i, 1, newTableItem(sym.getTypeName()));
        ui->tableWidget->setItem(i, 2, newTableItem(sym.scope->getDisplayName()));
        ui->tableWidget->setItem(i, 3, newTableItem(sym.isArray ? "Sim" : "Não"));
        ui->tableWidget->setItem(i, 4, newTableItem(sym.isFunction ? "Sim" : "Não"));
        ui->tableWidget->setItem(i, 5, newTableItem(sym.isParameter ? "Sim" : "Não"));
        ui->tableWidget->setItem(i, 6, newTableItem(sym.isInitialized? "Sim" : "Não"));
        ui->tableWidget->setItem(i, 7, newTableItem(sym.isReferenced ? "Sim" : "Não"));
    }

    auto root = semantic->copyScopeTree();
    QTreeWidgetItem* top = addRecursive(root.get());
    ui->treeWidget->addTopLevelItem(top);
    ui->treeWidget->expandAll();
}

void ResultsWindow::setAssemblyText(const QString& text)
{
    ui->assemblyView->setPlainText(text);
    ui->tabWidget->setTabEnabled(1, !text.isEmpty());
    ui->tabWidget->setCurrentIndex(text.isEmpty() ? 0 : 1);
}

void ResultsWindow::clear()
{
    ui->tableWidget->clearContents();
    ui->tableWidget->setRowCount(0);
    ui->treeWidget->clear();
}

QTableWidgetItem* ResultsWindow::newTableItem(const std::string& text) const
{
    auto item = new QTableWidgetItem(text.c_str());
    item->setFlags(Qt::ItemIsSelectable | Qt::ItemIsEnabled);
    return item;
}

QTreeWidgetItem* ResultsWindow::newTreeItem(const std::string& path, const std::string& tag) const
{
    auto item = new QTreeWidgetItem(QStringList {path.c_str(), tag.c_str()});
    item->setFlags(Qt::ItemIsSelectable | Qt::ItemIsEnabled);
    return item;
}

QTreeWidgetItem* ResultsWindow::addRecursive(ScopeTreeNode* node) const
{
    QTreeWidgetItem* item = nullptr;
    if(node) {
        if(node->tags.name == "global") {
            item = newTreeItem(node->getDisplayName(), "global");
        }
        else if(node->tags.isFunction) {
            item = newTreeItem(node->getDisplayName(), "function");
        }
        else {
            item = newTreeItem(node->getDisplayName(), "control");
        }
        for(auto child : node->children) {
            item->addChild(addRecursive(child));
        }
    }
    return item;
}

void ResultsWindow::actionSave()
{
    QFileDialog dialog(this);
    dialog.setDefaultSuffix(".asm");
    dialog.setNameFilter("Código assembly (*.asm);;Todos os arquivos (*)");
    dialog.setFileMode(QFileDialog::ExistingFile);
    dialog.setAcceptMode(QFileDialog::AcceptSave);
    dialog.exec();
    if(!dialog.selectedFiles().empty()) {
        QString filename = dialog.selectedFiles()[0];
        QFile file(filename);
        if(file.open(QFile::WriteOnly)) {
            auto text = ui->assemblyView->toPlainText();
            file.write(text.toUtf8());
        }
        else {
            // Erro na abertura
            QMessageBox::critical(this, QString("Erro abrindo \"%1\"").arg(filename), "Não foi possível abrir o arquivo para escrita");
        }
    }
}
