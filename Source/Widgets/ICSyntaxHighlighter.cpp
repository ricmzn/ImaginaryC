#include "ICSyntaxHighlighter.h"

struct HighlightRule {
    QRegExp regex;
    QString tag;
    QColor color;
    bool italic;
    bool bold;
};

static QTextCharFormat ruleToFormat(const HighlightRule& rule)
{
    QTextCharFormat format;
    int weight = rule.bold ? QFont::Bold : QFont::Normal;
    format.setForeground(rule.color);
    format.setFontItalic(rule.italic);
    format.setFontWeight(weight);
    return format;
}

#define RULE(regexp, color, italic, bold, tag)\
HighlightRule {\
    QRegExp(regexp),\
    QString(tag),\
    QColor(color),\
    bool(italic),\
    bool(bold)\
}

static HighlightRule highlightRules[] = {
    // Inteiros, floats e bools
    RULE("\\b(0x|0b)?[0-9]+\\b", Qt::darkBlue, false, false, ""),
    RULE("\\b[0-9]+\\.[0-9]+\\b", Qt::darkBlue, false, false, ""),
    RULE("\\b(true|false)\\b", Qt::darkBlue, true, false, ""),
    // Strings
    RULE("\".*\"", Qt::darkGreen, false, false, ""),
    // Palavras reservadas
    RULE("\\b(if|else|for|do|while|break|continue|return|int|float|string|char|bool|void)\\b", Qt::darkYellow, false, false, ""),
    // Comentários
    RULE("//.*$", Qt::darkGray, true, false, "CLINE"),
    RULE("/\\*.*\\*/", Qt::darkGray, true, false, "CLINE"),
    RULE("/\\*", Qt::darkGray, true, false, "CBEGIN"),
    RULE("\\*/", Qt::darkGray, true, false, "CEND"),
};

const auto commentFormat = ruleToFormat(RULE("//.*$", Qt::darkGray, true, false, "CLINE"));

#undef RULE

enum BlockState
{
    COMMENT = 1
};

/*****************/
/* COMEÇA CÓDIGO */
/*****************/

ICSyntaxHighlighter::ICSyntaxHighlighter(QObject* parent)
    : QSyntaxHighlighter(parent)
{
    highlightComments = false;
}

ICSyntaxHighlighter::~ICSyntaxHighlighter()
{}

void ICSyntaxHighlighter::highlightBlock(const QString& text)
{
    for(HighlightRule rule : highlightRules) {
        bool isComment = (rule.tag == "CBEGIN" || rule.tag == "CEND" || rule.tag == "CLINE");
        rule.regex.setMinimal(!isComment);
        const auto& format = ruleToFormat(rule);
        int index = rule.regex.indexIn(text);

        while(index >= 0) {
            if(highlightComments) {
                if(previousBlockState() == COMMENT) {
                    if(rule.tag == "CEND") {
                        setFormat(0, index, commentFormat);
                        setCurrentBlockState(0);
                    }
                    else {
                        setCurrentBlockState(COMMENT);
                        setFormat(0, text.length(), commentFormat);
                        return;
                    }
                }
                if(rule.tag == "CBEGIN") {
                    setFormat(index, text.length() - index, commentFormat);
                    setCurrentBlockState(COMMENT);
                    return;
                }
            }
            int length = rule.regex.matchedLength();
            setFormat(index, length, format);
            index = rule.regex.indexIn(text, index + length);
        }
    }
}
