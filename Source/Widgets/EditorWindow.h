#ifndef EDITORWINDOW_H
#define EDITORWINDOW_H

#include <QMainWindow>
#include <QMessageBox>

class QTextBlock;
class QComboBox;
class QSyntaxHighlighter;
class ResultsWindow;

namespace Ui {
class EditorWindow;
}

class EditorWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit EditorWindow(QWidget* parent = 0);
    ~EditorWindow();

    QMessageBox::StandardButton confirmSaveDialog();

    void openFile(const QString& path);
    void setCompiler(const QString& name);

private slots:
    void actionNewFile();
    void actionOpenFile();
    void actionSaveFile();
    void actionCompile();

    void setDirty(bool value = true);
    void updateWindowTitle();
    void clearErrors();

    void openResultsWindow();

private:
    Ui::EditorWindow* ui;
    QComboBox* compilerSelection;
    QSyntaxHighlighter* highlighter;
    ResultsWindow* resultsWindow;
    bool isDirty;
};

#endif // EDITORWINDOW_H
