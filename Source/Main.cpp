#include <QApplication>
#include <QCommandLineParser>
#include <QTextStream>

#include "CompilerFactories.h"
#include "Widgets/EditorWindow.h"

int main(int argc, char** argv)
{
    int status = 1;
    QApplication app(argc, argv);
    app.setApplicationName("IDE");
    app.setOrganizationName("ImaginaryC");

    QCommandLineParser parser;
    parser.setApplicationDescription("\nTrabalho da matéria de compiladores"
                                     "\nAlunos: Rafael Schmitt e Ricardo Maes");

    parser.addOption({{"I", "ide"}, "Abre em modo IDE (Padrão)"});
    parser.addOption({{"c", "compiler"}, "Compilador a ser usado", "nome", "bip"});
    parser.addOption({{"l", "listCompilers"}, "Lista todos os compiladores disponíveis"});
    parser.addOption({{"h", "help"}, "Exibe esse texto de ajuda"});
    parser.addPositionalArgument("entrada", "Arquivo a ser aberto ou compilado", "[entrada]");

    bool success = parser.parse(app.arguments());
    QStringList args = parser.positionalArguments();
    QTextStream out(stdout);
    QTextStream err(stderr);

    // Texto de ajuda
    if(!success || parser.isSet("help")) {
        out << parser.helpText();
        if(success) {
            status = 0;
        }
    }
    // Listar compiladores
    else if(success && parser.isSet("listCompilers")) {
        out << QString("Compiladores disponíveis:\n"
                       "");
        for(auto compiler : CompilerFactories::get()) {
            out << QString("  %1 - %2\n")
                   .arg(compiler->getName())
                   .arg(compiler->getDescription());
        }
        status = 0;
    }
    // Abrir IDE
    else if(success && (args.size() == 0 || parser.isSet("ide"))) {
        EditorWindow window;
        window.show();
        if(args.size()) {
            window.openFile(args[0]);
        }
        window.setCompiler(parser.value("compiler"));
        status = app.exec();
    }
    // Compilar arquivo individual
    else if(success && args.size() > 0) {
        auto compilers = CompilerFactories::get();
        auto predicate = [&](CompilerFactories::CompilerFactoryPtr compiler) {
            return parser.value("compiler") == QString(compiler->getName());
        };
        auto iter = std::find_if(compilers.begin(), compilers.end(), predicate);
        if(iter != compilers.end()) {
            auto instance = (*iter)->create();
            // TODO
            // instance->compile(code, printer);
            err << QString("Não implementado\n");
            status = 2;
        }
        else {
            err << QString("Compilador inválido! Use %2 -l para listar os compiladores disponíveis\n")
                   .arg(argv[0]);
        }
    }
    return status;
}
