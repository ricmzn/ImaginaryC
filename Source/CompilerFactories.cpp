#include "CompilerFactories.h"

#include <QScopedPointer>

#include "Compiler/BaseCompiler.h"
#include "Compiler/BipCompiler.h"

BaseCompilerFactory::BaseCompilerFactory(QObject* parent)
    : QObject(parent)
{}

std::shared_ptr<BaseCompiler> BaseCompilerFactory::create() const
{
    return std::make_shared<BaseCompiler>();
}

const char* BaseCompilerFactory::getName() const
{
    return "default";
}

const char* BaseCompilerFactory::getDescription() const
{
    return "Somente verificação semântica";
}


BipCompilerFactory::BipCompilerFactory(QObject* parent)
    : BaseCompilerFactory(parent)
{}

std::shared_ptr<BaseCompiler> BipCompilerFactory::create() const
{
    return std::make_shared<BipCompiler>();
}

const char* BipCompilerFactory::getName() const
{
    return "bip";
}

const char* BipCompilerFactory::getDescription() const
{
    return "UNIBALI BIP - Versão IV";
}


namespace CompilerFactories {
    // Lista de compiladores
    static const CompilerFactoryList factories {
        std::make_shared<BaseCompilerFactory>(),
        std::make_shared<BipCompilerFactory>()
    };
    // Acesso à lista
    const CompilerFactoryList& get()
    {
        return factories;
    }
}
