#ifndef SEMANTICINTERPRETER_H
#define SEMANTICINTERPRETER_H

#include <string>
#include <vector>
#include <memory>
#include <stack>

#include "Parser/Semantico.h"
#include "Instruction.h"

/// @brief Backend padrão de compilação que só verifica erros no código
class BaseCompiler : public Semantico
{
public:
    typedef Symbol::Type VarType;
    typedef Instruction::Type IType;
    typedef Expression::Operation EOperation;

protected:
    std::vector<Instruction> instructions;
    std::vector<SymbolPtr> symbols;
    ScopeTreeNode* globalScope;

    // Ponteiro para o escopo atual
    ScopeTreeNode* currentScope;

    // Caminho atual do escopo
    std::vector<int> currentPath;

    // Símbolo atual em processo de declaração
    Symbol tempSymbol;

    // Último escopo declarado
    Symbol scopeSymbol;

    // Próxima operação
    std::string op;

    // Variável alvo da próxima operação
    SymbolPtr opTarget;

    // Endereço de leitura dentro de vetor
    int arrayOffset;

    // Expressões sendo construídas atualmente
    std::stack<Expression> exprStack;

    // Elemento atual da expressão
    Expression* expr;

public:
    BaseCompiler();
    virtual ~BaseCompiler();

    virtual std::string compile(std::string source, std::shared_ptr<ILogPrinter> logPrinter);
    virtual void executeAction(int action, const Token* token) override;

    std::vector<Symbol> getSymbols() const;
    std::shared_ptr<ScopeTreeNode> copyScopeTree() const;

protected:
    virtual void clear();
    virtual void started();
    virtual void finished();

    void warning(const std::string& message, int position);
    void error(const std::string& message, int position);
    ILogPrinter* printer;

    virtual SymbolPtr addSymbol(Symbol symbol, const Token* token);
    SymbolPtr requireSymbol(ScopeTreeNode* scope, const Token* token);
    SymbolPtr findSymbol(ScopeTreeNode* scope, const std::string& name);
    SymbolPtr lastSymbol();
};

#endif // SEMANTICINTERPRETER_H
