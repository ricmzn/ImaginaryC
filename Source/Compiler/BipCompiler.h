#ifndef BIPCOMPILER_H
#define BIPCOMPILER_H

#include <sstream>
#include "BaseCompiler.h"

class BipCompiler : public BaseCompiler
{
public:
    std::string compile(std::string source, std::shared_ptr<ILogPrinter> logPrinter) override;

protected:
    std::stringstream output;

    SymbolPtr addSymbol(Symbol symbol, const Token* token) override;

    void clear() override;
    void started() override;
    void finished() override;

    void outputVariables();
    void outputInstructions();
};

#endif // BIPCOMPILER_H
