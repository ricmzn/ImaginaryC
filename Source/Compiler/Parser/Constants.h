#ifndef CONSTANTS_H
#define CONSTANTS_H

enum TokenId 
{
    EPSILON  = 0,
    DOLLAR   = 1,
    t_void = 2,
    t_for = 3,
    t_do = 4,
    t_while = 5,
    t_if = 6,
    t_else = 7,
    t_read = 8,
    t_write = 9,
    t_return = 10,
    t_TOKEN_11 = 11, //"="
    t_TOKEN_12 = 12, //"+="
    t_TOKEN_13 = 13, //"-="
    t_TOKEN_14 = 14, //"*="
    t_TOKEN_15 = 15, //"/="
    t_TOKEN_16 = 16, //"%="
    t_TOKEN_17 = 17, //"+"
    t_TOKEN_18 = 18, //"-"
    t_TOKEN_19 = 19, //"*"
    t_TOKEN_20 = 20, //"/"
    t_TOKEN_21 = 21, //"%"
    t_TOKEN_22 = 22, //"<"
    t_TOKEN_23 = 23, //">"
    t_TOKEN_24 = 24, //"<="
    t_TOKEN_25 = 25, //">="
    t_TOKEN_26 = 26, //"=="
    t_TOKEN_27 = 27, //"!="
    t_TOKEN_28 = 28, //"++"
    t_TOKEN_29 = 29, //"--"
    t_TOKEN_30 = 30, //"&&"
    t_TOKEN_31 = 31, //"||"
    t_TOKEN_32 = 32, //"!"
    t_TOKEN_33 = 33, //"<<"
    t_TOKEN_34 = 34, //">>"
    t_TOKEN_35 = 35, //"&"
    t_TOKEN_36 = 36, //"|"
    t_TOKEN_37 = 37, //"^"
    t_TOKEN_38 = 38, //"~"
    t_int = 39,
    t_hex = 40,
    t_bin = 41,
    t_float = 42,
    t_string = 43,
    t_bool = 44,
    t_char = 45,
    t_type = 46,
    t_TOKEN_47 = 47, //"."
    t_TOKEN_48 = 48, //":"
    t_TOKEN_49 = 49, //","
    t_TOKEN_50 = 50, //";"
    t_TOKEN_51 = 51, //"("
    t_TOKEN_52 = 52, //")"
    t_TOKEN_53 = 53, //"["
    t_TOKEN_54 = 54, //"]"
    t_TOKEN_55 = 55, //"{"
    t_TOKEN_56 = 56, //"}"
    t_id = 57
};

const int STATES_COUNT = 117;

extern int SCANNER_TABLE[STATES_COUNT][256];

extern int TOKEN_STATE[STATES_COUNT];

extern const char *SCANNER_ERROR[STATES_COUNT];

const int FIRST_SEMANTIC_ACTION = 114;

const int SHIFT  = 0;
const int REDUCE = 1;
const int ACTION = 2;
const int ACCEPT = 3;
const int GO_TO  = 4;
const int ERROR  = 5;

extern const int PARSER_TABLE[309][215][2];

extern const int PRODUCTIONS[131][2];

extern const char *PARSER_ERROR[309];

#endif
