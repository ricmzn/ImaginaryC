#ifndef SCOPETREE_H
#define SCOPETREE_H

#include <vector>
#include <string>
#include <cstring>

#include "Compiler/Symbol.h"

struct ScopeTreeNode
{
    // Variáveis
    const int id;
    int indexFromParent;
    ScopeTreeNode* parent;
    std::vector<ScopeTreeNode*> children;
    std::vector<SymbolPtr> variables;
    std::vector<int> fullPath;
    Symbol tags;

    ScopeTreeNode(ScopeTreeNode* parent, const Symbol& tags);
    ~ScopeTreeNode();

    ScopeTreeNode* parsePath(const std::vector<int>& relativePath);
    ScopeTreeNode* getRootNode();
    std::string getDisplayName() const;
    std::string getPathString() const;
    bool isUnique(const Symbol& sym, Symbol* found = nullptr) const;

    ScopeTreeNode* copyNode() const;
    bool operator==(const ScopeTreeNode& other) const;
};
#endif // SCOPETREE_H
