#ifndef LOGPRINTER_H
#define LOGPRINTER_H

struct ILogPrinter
{
    virtual void logWarning(const char* message) = 0;
};

#endif // LOGPRINTER_H
