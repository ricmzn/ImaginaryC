#include "Symbol.h"

#include <map>

#include "Compiler/ScopeTree.h"

static std::map<std::string, Symbol::Type> stringTypeMap {
    {"int", Symbol::Type::INT},
    {"float", Symbol::Type::FLOAT},
    {"string", Symbol::Type::STRING},
    {"bool", Symbol::Type::BOOL},
    {"char", Symbol::Type::CHAR},
    {"void", Symbol::Type::VOID},
};

std::string Symbol::getTypeName() const
{
    return toString(type);
}

std::string Symbol::getReferenceName() const
{
    auto scopePath = scope->getPathString();
    return scopePath + name;
}

Symbol::Type Symbol::parseType(std::string str)
{
    auto it = stringTypeMap.find(str);
    if(it != stringTypeMap.end()) {
        return it->second;
    }
    return Symbol::Type::INVALID;
}

std::string Symbol::toString(Symbol::Type type)
{
    std::string typeName = "invalid";
    for(auto pair : stringTypeMap) {
        if(pair.second == type) {
            typeName = pair.first;
            break;
        }
    }
    return typeName;
}

Symbol::Symbol(std::string name)
    : Symbol(name, int())
{}

Symbol::Symbol(std::string name, ValueType value)
    : type(Type::INVALID)
    , scope(nullptr)
    , name(name)
    , isArray(false)
    , isFunction(false)
    , isParameter(false)
    , isInitialized(false)
    , isReferenced(false)
    , tokenPos(-1)
    , value(value)
    , size(1)
{}
