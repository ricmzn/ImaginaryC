#ifndef INSTRUCTION_H
#define INSTRUCTION_H

#include <string>
#include <vector>

#include "Symbol.h"
#include "Expression.h"

struct Instruction
{
    enum class Type
    {
        LOAD,
        STORE,
        EXPR,
        BRANCH,
        JUMP,
        CALL,
        RETURN,
        IO_READ,
        IO_WRITE,

        INVALID
    };

    Type type;
    Symbol target;
    Expression::Operation exprOp;

    Instruction();
};

#endif // INSTRUCTION_H
