#ifndef EXPRESSION_H
#define EXPRESSION_H

#include <memory>
#include "Symbol.h"

struct Expression
{
    enum class Operation
    {
        NONE,
        ADD,
        SUB,
        EQ,
        NEQ,
        GT,
        GTE,
        LT,
        LTE,
        AND,
        OR,
        NOT,
        NEGATE,

        INVALID
    };

    Symbol target;
    Operation op;
    bool isDynamic;
    std::shared_ptr<Expression> next;

    Expression();
    Expression(Symbol var, Operation op = Operation::NONE);
    Expression(Symbol::ValueType val, Operation op = Operation::NONE);

    void solvePartial();
    bool solve(Symbol::ValueType* result);
};

typedef Expression::Operation ExpOp;

#endif // EXPRESSION_H
