#include "Expression.h"
#include <exception>

Expression::Expression()
    : Expression(Symbol::Void(), Operation::NONE)
{}

Expression::Expression(Symbol var, Expression::Operation op)
    : target(var)
    , op(op)
    , isDynamic(true)
    , next()
{}

Expression::Expression(Symbol::ValueType val, Expression::Operation op)
    : target(Symbol("", val))
    , op(op)
    , isDynamic(false)
    , next()
{}

void Expression::solvePartial()
{
    // "Compacta" todos os membros estáticos (conhecidos em momento de compilação) da expressão
    while(next) {
        if(!isDynamic) {
            int a = get<int>(target.value);
            int b = get<int>(next->target.value);
            // Operações unárias
            switch(op) {
                case Operation::NONE:
                    break;
                case Operation::NOT:
                    target.value = !a;
                    break;
                case Operation::NEGATE:
                    target.value = -a;
                    break;
                default:
                    throw std::invalid_argument("Operação unária mal formada!");
            }
            // Operações binárias
            switch(op) {
                case Operation::ADD:
                    target.value = a + b;
                    break;
                case Operation::SUB:
                    target.value = a - b;
                    break;
                case Operation::EQ:
                    target.value = a == b;
                    break;
                case Operation::NEQ:
                    target.value = a != b;
                    break;
                case Operation::GT:
                    target.value = a > b;
                    break;
                case Operation::GTE:
                    target.value = a >= b;
                    break;
                case Operation::LT:
                    target.value = a < b;
                    break;
                case Operation::LTE:
                    target.value = a <= b;
                    break;
                case Operation::AND:
                    target.value = a && b;
                    break;
                case Operation::OR:
                    target.value = a && b;
                    break;
                default:
                    throw std::invalid_argument("Operação binária mal-formada!");
            }
            // Remove a operação já feita da lista
            op = next->op;
            next = next->next;
        }
    }
}

bool Expression::solve(Symbol::ValueType* result)
{
    *result = 0;
    return true;
}
