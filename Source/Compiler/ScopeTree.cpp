#include "ScopeTree.h"

#include <algorithm>
#include <sstream>
#include <deque>

static int lastScopeTreeNodeID = 0;

ScopeTreeNode::ScopeTreeNode(ScopeTreeNode* parent, const Symbol& tags)
    : id(lastScopeTreeNodeID++)
    , indexFromParent(-1)
    , parent(parent)
    , tags(tags)
{
    // Adiciona esse nó na lista do seu pai se já não estiver contido
    if(parent) {
        auto& list = parent->children;
        if(std::find(list.begin(), list.end(), this) == list.end()) {
            indexFromParent = list.size();
            list.push_back(this);
        }
    }
}

ScopeTreeNode::~ScopeTreeNode()
{
    for(ScopeTreeNode* scope : children) {
        delete scope;
    }
}

ScopeTreeNode* ScopeTreeNode::parsePath(const std::vector<int> &relativePath)
{
    auto node = this;
    while(!relativePath.empty()) {
        if(relativePath[0] >= int(node->variables.size())) {
            return nullptr;
        }
        node = node->children[relativePath[0]];
    };
    return node;
}

ScopeTreeNode* ScopeTreeNode::getRootNode()
{
    auto node = this;
    while(node->parent) {
        node = node->parent;
    }
    return node;
}

std::string ScopeTreeNode::getDisplayName() const
{
    if(fullPath.empty()) {
        return std::string("[]");
    }

    // Monta a string do caminho a partir do vetor de índices
    auto iterator = fullPath.begin();
    std::stringstream ss;
    ss << "[";
    while(true) {
        ss << *iterator;
        ++iterator;
        if(iterator != fullPath.end()) {
            ss << ", ";
        }
        else {
            break;
        }
    }
    ss << "]";
    if(!tags.name.empty()) {
        ss << " '" << tags.name << "'";
    }
    return ss.str();
}

std::string ScopeTreeNode::getPathString() const
{
    if(fullPath.empty()) {
        return "";
    }

    std::deque<std::string> names;
    auto scope = this;
    while(scope->parent) {
        if(scope->tags.isFunction) {
            names.push_front(scope->tags.name);
        }
        else {
            names.push_front(scope->tags.name + std::to_string(scope->indexFromParent));
        }
        scope = scope->parent;
    }
    std::stringstream ss;
    for(std::string name : names) {
        ss << name << "_";
    }
    return ss.str();
}

bool ScopeTreeNode::isUnique(const Symbol& sym, Symbol* found) const
{
    if(tags.isFunction && sym.name == tags.name) {
        if(found) {
            *found = tags;
        }
        return false;
    }
    for(const auto& child : variables) {
        if(child->name == sym.name) {
            if(found) {
                *found = *child;
            }
            return false;
        }
    }
    if(parent) {
        return parent->isUnique(sym);
    }
    return true;
}

ScopeTreeNode* ScopeTreeNode::copyNode() const
{
    auto node = new ScopeTreeNode(nullptr, tags);
    node->fullPath = fullPath;
    node->variables = variables;
    node->indexFromParent = indexFromParent;
    node->children.reserve(children.size());
    for(auto& var : node->variables) {
        var->scope = node;
    }
    for(auto& child : children) {
        auto scope = child->copyNode();
        scope->parent = node;
        node->children.push_back(scope);
    }
    return node;
}

bool ScopeTreeNode::operator==(const ScopeTreeNode& other) const
{
    return id == other.id;
}
